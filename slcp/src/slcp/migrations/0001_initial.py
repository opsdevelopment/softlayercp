# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('companyName', models.CharField(max_length=100)),
                ('sl_acc_id', models.IntegerField(null=True)),
                ('sl_acc_brand_id', models.IntegerField(null=True)),
                ('sl_api_key', models.CharField(max_length=300, null=True)),
                ('sl_api_username', models.CharField(max_length=300, null=True)),
                ('sl_user_id', models.IntegerField(null=True)),
                ('firstName', models.CharField(max_length=100)),
                ('lastName', models.CharField(max_length=100)),
                ('address1', models.CharField(max_length=100)),
                ('postalCode', models.CharField(max_length=40)),
                ('city', models.CharField(max_length=40)),
                ('state', models.CharField(max_length=2)),
                ('country', models.CharField(max_length=2)),
                ('officePhone', models.CharField(max_length=40)),
                ('email', models.EmailField(max_length=254)),
                ('lateFeeProtectionFlag', models.BooleanField(default=False)),
                ('claimedTaxExemptTxFlag', models.BooleanField(default=False)),
                ('allowedPptpVpnQuantity', models.IntegerField(default=1)),
                ('isReseller', models.BooleanField(default=False)),
                ('accountStatusId', models.CharField(default=b'1001', max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('account_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='slcp.Account')),
                ('keyName', models.CharField(max_length=100)),
                ('longName', models.CharField(max_length=240)),
                ('name', models.CharField(max_length=100)),
                ('sl_brand_id', models.IntegerField(null=True)),
            ],
            bases=('slcp.account',),
        ),
    ]
