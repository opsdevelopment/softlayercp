# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slcp', '0004_auto_20151128_0219'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='master_account',
            field=models.ForeignKey(related_name='accounts', default=1, to='slcp.Master'),
        ),
    ]
