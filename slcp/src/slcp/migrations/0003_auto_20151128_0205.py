# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slcp', '0002_master'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='master',
            name='master_pwd',
        ),
        migrations.AddField(
            model_name='master',
            name='master_api_key',
            field=models.CharField(default='', max_length=120),
            preserve_default=False,
        ),
    ]
