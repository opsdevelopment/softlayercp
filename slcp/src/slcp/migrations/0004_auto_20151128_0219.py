# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slcp', '0003_auto_20151128_0205'),
    ]

    operations = [
        migrations.AlterField(
            model_name='master',
            name='master_api_key',
            field=models.CharField(max_length=140),
        ),
    ]
