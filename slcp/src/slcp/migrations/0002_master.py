# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slcp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Master',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('master_user', models.CharField(max_length=20)),
                ('master_pwd', models.CharField(max_length=40)),
                ('name', models.CharField(max_length=40)),
                ('description', models.TextField()),
            ],
        ),
    ]
