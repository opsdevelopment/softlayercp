from django.contrib import admin
import models


class MasterAdmin(admin.ModelAdmin):
    model = models.Master


class BrandAdmin(admin.ModelAdmin):
    model = models.Brand


class AccountAdmin(admin.ModelAdmin):
    model = models.Account


admin.site.register(models.Account, AccountAdmin)
admin.site.register(models.Brand, BrandAdmin)
admin.site.register(models.Master, MasterAdmin)

