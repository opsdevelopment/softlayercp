from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, Field
from crispy_forms.bootstrap import Accordion, AccordionGroup
from django import forms
import models


class AddAccountForm(ModelForm):
    class Meta:
        model = models.Account
        fields = ['companyName', 'firstName', 'lastName','email', 'address1', 'postalCode', 'city', 'state',
                  'country', 'officePhone', 'lateFeeProtectionFlag', 'claimedTaxExemptTxFlag',
                  'allowedPptpVpnQuantity', 'isReseller', 'accountStatusId', 'master_account']
        labels = {'isReseller': 'Reseller',
                    'claimedTaxExemptTxFlag': 'Claimed Tax Exempt',
                    'lateFeeProtectionFlag': 'Late fee protection'}
        widgets = {'accountStatusId': forms.HiddenInput, 'allowedPptpVpnQuantity': forms.HiddenInput,
                   'lateFeeProtectionFlag': forms.HiddenInput, 'claimedTaxExemptTxFlag': forms.HiddenInput,
                   'master_account':forms.HiddenInput}
    #pick brand
    brand = forms.ModelChoiceField(queryset=models.Brand.objects.all(), required=False)

    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-horizontal'
    helper.form_show_errors = True
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-4'
    helper.add_input(Submit('create', 'Create', css_class='btn-primary'))
    helper.layout = Layout(
        Accordion(
            AccordionGroup('Brand','brand', css_class='col-sm-4'),
        ),
        Accordion(
            AccordionGroup('Account Info', *Meta.fields),
        )
    )

    def __init__(self, *args, **kwargs):
        super(AddAccountForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        self.fields['brand'].queryset = models.Brand.objects.filter(master_account=instance.master_account)
        # editing existing account
        if instance and instance.sl_user_id:
            self.fields['brand'].initial = models.Brand.objects.get(sl_brand_id=instance.sl_acc_brand_id)
            #self.fields['brand'].required = False
            self.fields['brand'].widget.attrs['disabled'] = True


class AddBrandForm(ModelForm):
    class Meta:
        model = models.Brand
        brand_fields = ['keyName', 'name', 'longName']
        fields = brand_fields + AddAccountForm.Meta.fields
        widgets = AddAccountForm.Meta.widgets
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-horizontal'
    helper.form_show_errors = True
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-4'
    helper.add_input(Submit('create', 'Create', css_class='btn-primary'))
    helper.layout = Layout(
        Accordion(
            AccordionGroup('Brand Info', *Meta.brand_fields),
        ),
        Accordion(
            AccordionGroup('Account Info', *AddAccountForm.Meta.fields),
        )
    )


class MasterSwitchForm(forms.Form):
    master_id = forms.ChoiceField(label="Master Account:",
                                  widget=forms.Select(attrs={"onChange": "this.form.submit()",
                                                             "class": "form-control"}),
                                  choices=[(o.id, o.name) for o in models.Master.objects.all()],
                                  )