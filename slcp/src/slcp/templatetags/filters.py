from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(is_safe=True)
def getval(dic, val):
    try:
        return dic[val]
    except:
        return None


@register.filter(name="end_user", is_safe=True)
def get_end_user(company_name):
    ind = company_name.rfind('-')
    if -1 == ind:
        return 'N/A'
    return company_name[ind + 1:].strip()
