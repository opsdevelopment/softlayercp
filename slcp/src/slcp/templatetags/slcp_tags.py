from django import template
from slcp.forms import MasterSwitchForm
register = template.Library()


@register.assignment_tag()
def master_form(request):
    return MasterSwitchForm(initial={'master_id': request.session['master_id']})
