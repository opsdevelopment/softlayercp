from django.views import generic
from .forms import AddAccountForm, AddBrandForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators  import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.forms.forms import NON_FIELD_ERRORS
import models
from decorators import update_master_account
import SoftLayer, logging
import SoftLayer.API
from django.core.cache import cache
from django.shortcuts import get_object_or_404

log = logging.getLogger('SLCP')


@login_required()
def dashboard(request):
    return HttpResponseRedirect(reverse("list_accounts"))


@login_required()
@update_master_account
def create_brand(request):
    if request.method == 'POST':
        form = AddBrandForm(request.POST)
        if form.is_valid():
            brand = form.save()
            try:
                brand.create()
                log.info(" (%s) created brand %s" % (brand, request.user))
                return HttpResponseRedirect(reverse("list_brands"))
            except SoftLayer.SoftLayerAPIError, e:
                err = u"Error: %s, %s" % (e.faultCode, e.faultString)
                brand.delete()
                log.error(err)
                form._errors[NON_FIELD_ERRORS] = form.error_class([err])
    else:
        form = AddBrandForm(initial={'accountStatusId': 1001, 'allowedPptpVpnQuantity': 1,
                                    'lateFeeProtectionFlag': False, 'claimedTaxExemptTxFlag': False,
                                    "master_account": request.session['master_id']})

    return render(request, 'create_brand.html', {'form': form})


@login_required()
@update_master_account
def create_account(request, account_id=0):
    if account_id:
        account = get_object_or_404(models.Account, sl_acc_id=account_id)
    else:
        master = models.Master.objects.get(pk=request.session['master_id'])
        account = models.Account(master_account=master)

    if request.method == 'POST':
        form = AddAccountForm(request.POST or None, instance=account)
        if form.is_valid():
            acc = form.save()
            brand = form.cleaned_data['brand']
            try:
                if account_id:
                    acc.update()
                else:
                    acc.create(brand)
                return HttpResponseRedirect(reverse("list_accounts"))
            except SoftLayer.SoftLayerAPIError, e:
                err = u"Error: %s, %s" % (e.faultCode, e.faultString)
                if not account_id:
                    acc.delete()
                log.error(err)
                form._errors[NON_FIELD_ERRORS] = form.error_class([err])
    else:
        form = AddAccountForm(initial={'accountStatusId': 1001, 'allowedPptpVpnQuantity': 1,
                                       'lateFeeProtectionFlag': False, 'claimedTaxExemptTxFlag': False},
                              instance=account)

    return render(request, 'create_account.html', {'form': form})


@login_required()
@update_master_account
def list_brands(request):
    return render(request, 'list_brands.html', {
        'brands': models.Brand.objects.filter(master_account_id=request.session['master_id'])
    })


@login_required()
@update_master_account
def list_accounts(request):
    master_id = request.session['master_id']
    return render(request, 'list_accounts.html',
                  {'accounts': models.Account.objects.filter(master_account__pk=master_id).order_by('-sl_acc_id'),
                   'brands': models.Brand.objects.filter(master_account__pk=master_id)})


@login_required()
@update_master_account
def report_invoices(request):
    master_id = request.session['master_id']
    res = cache.get('stats %s' % master_id)
    if res is None:
        res = models.get_invoices(master_id)
        cache.set('stats %s' % master_id, res)

    return render(request, 'reports/invoices.html', {'invoices': res})
