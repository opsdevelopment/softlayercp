from django.db import models
from django.core import serializers
import json, SoftLayer
import datetime, dateutil.parser as dateparser

import logging
log = logging.getLogger("slcp.models")


class Master(models.Model):
    master_user = models.CharField(max_length=20)
    master_api_key = models.CharField(max_length=140)
    name = models.CharField(max_length=40)
    description = models.TextField()

    def __repr__(self):
        return "Master account: %s, username:%s" % (self.name, self.master_user)

    def __unicode__(self):
        return self.name


class Account(models.Model):
    companyName = models.CharField(max_length=100)
#   customer account ID in SL DB
    sl_acc_id = models.IntegerField(null=True)
#   brand_id in SL DB
    sl_acc_brand_id = models.IntegerField(null=True)

    master_account = models.ForeignKey(Master, related_name="accounts", null=False, default=1)

    sl_api_key = models.CharField(max_length=300, null=True)
    sl_api_username = models.CharField(max_length=300, null=True)
    sl_user_id = models.IntegerField(null=True)

    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    address1 = models.CharField(max_length=100)
    postalCode = models.CharField(max_length=40)
    city = models.CharField(max_length=40)
    state = models.CharField(max_length=2)
    country = models.CharField(max_length=2)
    officePhone = models.CharField(max_length=40)
    email = models.EmailField()
    lateFeeProtectionFlag = models.BooleanField(default=False)
    claimedTaxExemptTxFlag = models.BooleanField(default=False)
    allowedPptpVpnQuantity = models.IntegerField(default=1)
    isReseller = models.BooleanField(default=False)
#   1001 - Active
    accountStatusId = models.CharField(max_length=40, default="1001")

    def __str__(self):
        return "%s: %s %s (%s) %s" % (self.sl_acc_id, self.firstName, self.lastName, self.companyName, self.city)

    def get_sl_client(self):
        return SoftLayer.Client(username=self.master_account.master_user, api_key=self.master_account.master_api_key)

    def to_json(self):
        str_serialized = serializers.serialize('json', [self])
        obj = json.loads(str_serialized)
        return obj[0]['fields']

    def create(self, brand):
        client = self.get_sl_client()
        jsn = self.to_json()
        jsn['brandId'] = brand.sl_brand_id
        self.sl_acc_brand_id = brand.sl_brand_id
        result = client['Brand'].createCustomerAccount(jsn)
        self.sl_api_key = result['masterUser']['apiAuthenticationKeys'][0]['authenticationKey']
        self.sl_api_username = result['masterUser']['username']
        self.sl_user_id = result['masterUser']['apiAuthenticationKeys'][0]['userId']
        self.sl_acc_id = result['id']
        self.save()
        log.debug(" (%s) created account %s" % (brand, self.sl_acc_id))


    def update(self):
        # here we use customer's api key and username,
        # in all other places we use master account credentials

        client = SoftLayer.Client(username=self.sl_api_username, api_key=self.sl_api_key)
#       changeData = {'companyName':self.companyName, 'lastName':self.lastName, 'firstName':self.lastName,
#                       address1:}
        client['SoftLayer_User_Customer'].editObject(self.to_json(), id=self.sl_user_id)
        log.debug("Updated account %s" % self)

    def create_ticket(self):
        client = self.get_sl_client()
        currentUser = client['Account'].getCurrentUser()
        msg = """
            Please enable 'Add customers' feature( Agent`s portal -> Customers -> Add customer) for Account #%s under
            Brand #%s .
            """ % (self.sl_acc_id, self.sl_acc_brand_id)
        new_ticket = {
            # Sales request. see client['Ticket_Subject'].getAllObjects()
            'subjectId': 1002,
            'assignedUserId':  currentUser['id']
        }
        created_ticket = client['Ticket'].createStandardTicket(new_ticket, msg)
        return created_ticket


class Brand(Account):
    keyName = models.CharField(max_length=100, blank=False)
    longName = models.CharField(max_length=240)
    name = models.CharField(max_length=100)
    sl_brand_id = models.IntegerField(null=True)

    def to_json(self):
        str_serialized = serializers.serialize('json', [self])
        fields = json.loads(str_serialized)[0]['fields']
        fields['account'] = self.account_ptr.to_json()
        return fields

    def __str__(self):
        return "%s(%s)" % (self.name, self.sl_brand_id)

    def create(self):
        client = self.get_sl_client()
        result = client['Brand'].createObject(self.to_json())
        self.sl_brand_id = result['id']
        try:
            ticket = self.create_ticket()
            log.info("Ticket (%s) created to enable 'Add customers'" % ticket['id'])
        except Exception, e:
            log.error("Failed to create ticket for brand (%s): %s" % (self.sl_brand_id, e))
        self.save()

    def create_ticket(self):
        client = self.get_sl_client()
        currentUser = client['Account'].getCurrentUser()
        msg = """
            Please enable 'Add customers' feature( Agent`s portal -> Customers -> Add customer) for Brand #%s.
            """ % self.sl_brand_id
        new_ticket = {
            # Sales request. see client['Ticket_Subject'].getAllObjects()
            'subjectId': 1002,
            'assignedUserId': currentUser['id']
        }
        created_ticket = client['Ticket'].createStandardTicket(new_ticket, msg)
        return created_ticket

    @classmethod
    def get_all_brands(cls, master_id=0):
        master = Master.objects.get(pk=master_id)
        client = SoftLayer.Client(username=master.master_user, api_key=master.master_api_key)
        mask = 'ownedBrands.allOwnedAccounts.ownedBrands'
        res = client['SoftLayer_Account'].getObject(mask=mask)
        brand_dicts = res['ownedBrands'][0]['allOwnedAccounts']

        brands = list(Brand.objects.filter(master_account__pk=master_id))
        for b in brand_dicts:
            if not b['ownedBrands']:
                continue
            try:
                brand_info = b['ownedBrands'][0]
                brand = Brand(keyName=brand_info['keyName'], longName=brand_info['longName'],
                              name=brand_info['name'], sl_brand_id=brand_info['id'],
                              companyName=b['companyName'],
                              sl_acc_id=b['id'],
                              sl_acc_brand_id=brand_info['id'],
                              firstName=b['firstName'],
                              lastName=b['lastName'],
                              address1=b['address1'],
                              postalCode=b['postalCode'],
                              city=b['city'],
                              state=b['state'],
                              country=b['country'],
                              officePhone=b['officePhone'],
                              email=b['email'],
                              isReseller=b['isReseller'],
                              accountStatusId=b['accountStatusId'],
                              lateFeeProtectionFlag=b['lateFeeProtectionFlag'],
                              claimedTaxExemptTxFlag=b['claimedTaxExemptTxFlag'],
                              allowedPptpVpnQuantity=b['allowedPptpVpnQuantity']
                )
    #           save only if there is no such brand in DB already
                if not filter(lambda x: x.sl_brand_id is not None and int(x.sl_brand_id) == brand_info['id'], brands):
                    brand.save()
                    brands.append(brand)
                    log.info("Saving brand %s to database" % brand)
            except KeyError, e:
                log.info("Error when fetching brand(%s): %s: " % (brand, e))

        return brands

    @classmethod
    def get_accounts(cls, brand_id, master_id=0):
        master = Master.objects.get(pk=master_id)
        client = SoftLayer.Client(username=master.master_user, api_key=master.master_api_key)
        sl_accounts = client['SoftLayer_Brand'].getAllOwnedAccounts(id=brand_id)
        accounts = list(Account.objects.filter(master_account=master))
        for acc in sl_accounts:
            if not filter(lambda x: int(x.sl_acc_brand_id) == acc['id'], accounts):
                account = Account(
                    companyName=acc['companyName'],
                    sl_acc_id=acc['id'],
                    sl_acc_brand_id=brand_id,
                    firstName=acc['firstName'],
                    lastName=acc['lastName'],
                    address1=acc['address1'],
                    postalCode=acc['postalCode'],
                    city=acc['city'],
                    state=acc['state'],
                    country=acc['country'],
                    officePhone=acc['officePhone'],
                    email=acc['email'],
                    isReseller=acc['isReseller'],
                    accountStatusId=acc['accountStatusId'],
                    lateFeeProtectionFlag=acc['lateFeeProtectionFlag'],
                    claimedTaxExemptTxFlag=acc['claimedTaxExemptTxFlag'],
                    allowedPptpVpnQuantity=acc['allowedPptpVpnQuantity']
                )
                account.save()
                log.debug("Saving account %s to database" % account)
                accounts.append(account)
        return accounts


def get_invoices(master_id=0):

    start = datetime.datetime.now()
    end = datetime.datetime.now()
    mask = 'invoices'
#   fucking magic goes here
    inv_mask = "items[billingItem[id,item[keyName]]], invoiceTotalAmount"
    brands = Brand.get_all_brands(master_id)
    invoices = {}
    f = {
        'invoices': {
            'createDate': {
                'operation': 'betweenDate',
                'options': [
                    {'name': 'startDate', 'value': [start.strftime('m/d/Y H:i:s')]},
                    {'name': 'endDate', 'value': [end.strftime('m/d/Y H:i:s')]}
                ]
            }
        }
    }
    master = Master.objects.get(pk=master_id)
    client = SoftLayer.Client(username=master.master_user, api_key=master.master_api_key)
    for brand in brands:
        try:
            brand_accounts = client['SoftLayer_Brand'].getAllOwnedAccounts(id=brand.sl_brand_id, mask=mask, filter=f)
        except SoftLayer.SoftLayerAPIError,e:
            log.error("Exception in getAllOwnedAccounts for brand (%s): %s" % (brand, e))
            continue

        invoices[brand.sl_brand_id] = {}

        for account in brand_accounts:
            # skip account if there is no invoices
            if not len(account['invoices']):
                continue

            invoices[brand.sl_brand_id][account['id']] = {}
            for invoice in account['invoices']:
                #   Parse date to datetime format dateutil.parser.parse(datestring)
                if invoice['statusCode'] == 'CLOSED':
                    continue
                invoice['createDate'] = dateparser.parse(invoice['createDate'])
                invoices[brand.sl_brand_id][account['id']][invoice['id']] = {'invoice': invoice, 'bitems': [], 'total': 0}
                invoice_items = client['SoftLayer_Billing_Invoice'].getObject(id=invoice['id'], mask=inv_mask)

                for item in invoice_items['items']:
                    amount = float(item['oneTimeAfterTaxAmount']) + float(item['recurringAfterTaxAmount'])
                    if not amount:
                        continue
                    invoices[brand.sl_brand_id][account['id']][invoice['id']]['bitems'].append(item)
                    invoices[brand.sl_brand_id][account['id']][invoice['id']]['total'] += amount
#           filter empty accounts
            if not len(invoices[brand.sl_brand_id][account['id']]):
                invoices[brand.sl_brand_id].pop(account['id'])

#       filter empty brands
        if not len(invoices[brand.sl_brand_id]):
            invoices.pop(brand.sl_brand_id)
#       save stats in cache
#       cache.set('stats', res )
    return invoices