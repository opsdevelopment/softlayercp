from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import profiles.urls
import accounts.urls
from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.dashboard, name='home'),
#   url(r'^about/$', views.AboutPage.as_view(), name='about'),
    url(r'^create_brand/$', views.create_brand, name='create_brand'),
    url(r'^list_brands/$', views.list_brands, name='list_brands'),
    url(r'^create_account/$', views.create_account, name='create_account'),
    url(r'^update_account/(?P<account_id>\d+)', views.create_account, name='update_account'),
    url(r'^list_accounts/$', views.list_accounts, name='list_accounts'),
    url(r'^report_invoices/$', views.report_invoices, name='report_invoices'),
    url(r'^', include(accounts.urls, namespace='accounts')),
    url(r'^users/', include(profiles.urls, namespace='profiles')),
    url(r'^admin/', include(admin.site.urls)),

    #auth
    (r'^accounts/',  include(accounts.urls)),
    (r'^profile/',  include(profiles.urls)),
)

# User-uploaded files like profile pics need to be served in development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
