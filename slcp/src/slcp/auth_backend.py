
#Auth backend for Softlayer
from django.conf import settings
from django.contrib.auth import get_user_model
import SoftLayer,logging

User = get_user_model()

log = logging.getLogger('SLCP')

class SLAuthBackend(object):
    """
    Authenticate against the Softlayer CP

    Use the login name, and a hash of the password. For example:

    ADMIN_LOGIN = 'admin'
    ADMIN_PASSWORD = 'sha1$4e987$afbcf42e21bd417fb71db8c66b321e9fc33051de'
    """

    def authenticate(self, username=None, password=None):
        client = SoftLayer.Client(username=settings.SOFTLAYER_USER, api_key=settings.SOFTLAYER_API_KEY)
        user_customer = client['SoftLayer_User_Customer']
        try:
            result = user_customer.getPortalLoginToken(username, password)
            if not result:
                return None
            user = User.objects.get(username=username)
        except SoftLayer.SoftLayerAPIError:
            #not authenticated?
            return None
        except User.DoesNotExist:
            # User authenticated. Create a new user.
            user = User(username=username, password='Logme123!')
            user.is_staff = False
            user.save()
            log.info("Created user %s" % username)
        log.info("Authenticated user %s" % username)
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None