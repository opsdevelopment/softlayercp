from functools import wraps
from django.http import HttpResponseRedirect

"""
Checks that request.session['master_id'] isset to the right value,
before executing view.
When changing master account view doesn't get executed, we just redirect to the same page.
"""
def update_master_account(func=None):
    @wraps(func)
    def wrapped(request, *args, **kwargs):
        master_id = request.POST.get("master_id", 0)
        if master_id:
            request.session['master_id'] = master_id
            return HttpResponseRedirect(request.path)
        elif 'master_id' not in request.session:
            request.session['master_id'] = 1
        if func:
            return func(request, *args, **kwargs)
    return wrapped
